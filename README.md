# Translations for Volia Armory web service
**Feel free to submit pull requests**, if you would like to help

translate a project or suggest a new language for a web service.

We'll check and push them to the master branch if the translation is correct.

The most active users can be rewarded by Volia support team,

just send them a list of your combined PRs. 

**Thanks for your cooperation!**


